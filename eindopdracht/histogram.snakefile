# create histogram
rule make_histogram:
    input:
        WT = 'zipped_vcf/WT.vcf',
        KO = 'merged/mergedKO.vcf'
    output:
        'graph.pdf'
    log:
        'logfiles/graph.pdf.log'
    shell:
        "Rscript histogram/genotype_variants_mouse_linux.R {input.WT} {input.KO} 2> {log}"
