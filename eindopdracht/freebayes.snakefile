# Detects variants
rule freebayes:
    input:
        fa = 'data/mm10.fa',
        fai = 'data/mm10.fa.fai',
        bam = 'sorted_data/{sample}.bam',
        bai = 'sorted_data/{sample}.bam.bai'
    output:
        'vcf_files/{sample}.vcf'
    log:
        'logfiles/vcf_files/{sample}.vcf.log'
    threads: 40
    shell:
        'freebayes-parallel <(fasta_generate_regions.py {input.fai} 100000) {threads} -f {input.fa} -b {input.bam} > {output} 2> {log}'

# Filter the qualtity score on > 30
rule filter_quality:
    input:
        'vcf_files/{sample}.vcf'
    output:
        'vcf_filtered/{sample}_QUAL.vcf'
    log:
        'logfiles/vcf_filtered/{sample}_QUAL.vcf.log'
    threads: 10
    shell:
        'vcffilter -f "QUAL > 30" {input} > {output} 2> {log}'

# Filter the dept on > 10
rule filter_dept:
    input:
        'vcf_filtered/{sample}_QUAL.vcf'
    output:
        'vcf_filtered/{sample}_DP.vcf'
    log:
        'logfiles/vcf_filtered/{sample}_DP.vcf.log'
    threads: 10
    shell:
        'vcffilter -f "DP > 10" {input} > {output} 2> {log}'

# Decompose the filtered results
rule decomposing:
    input:
        'vcf_filtered/{sample}_DP.vcf'
    output:
        'vcf_outputs/{sample}_filtered.vcf'
    log:
        'logfiles/vcf_outputs/{sample}_filtered.vcf.log'
    threads: 10
    shell:
        'vcfallelicprimitives -g {input} > {output} 2> {log}'

# Intersecting KO VCF files
rule intersect_vcf:
    input:
        'vcf_outputs/{sample}_filtered.vcf'
    output:
        'vcf_outputs/{sample}_filtered.vcf.gz'
    log:
        'logfiles/vcf_outputs/{sample}_filtered.vcf.gz.log'
    shell:
        'bgzip -i {input} 2> {log}'

# bgzip VCF files
rule bgzip_vcf:
    input:
        expand('vcf_outputs/{sample}_filtered.vcf.gz', sample=config['samples'])
    output:
        'zipped_vcf/{sample}.vcf.gz'
    log:
        'logfiles/zipped_vcf/{sample}.vcf.gz.log'
    shell:
        'zcat {input} | bgzip -c > {output} 2> {log}'
