# create sequence dictionary with picard
rule create_sequence_dict:
    input:
        picard = 'scripts/picard.jar',
        fa = 'data/mm10.fa'
    output:
        'merged/reference.dict'
    log:
        'logfiles/merged/reference.dict.log'
    shell:
        'java -jar {input.picard} CreateSequenceDictionary R={input.fa} O={output} 2> {log}'

# merge the KO VCF files
rule merge_vcf:
    input:
        picard = 'scripts/picard.jar',
        KO1 = 'zipped_vcf/KO1.vcf.gz',
        KO2 = 'zipped_vcf/KO2.vcf.gz',
        KO3 = 'zipped_vcf/KO3.vcf.gz',
        ref = 'merged/reference.dict'
    output:
        'merged/mergedKO.vcf.gz'
    log:
        'logfiles/merged/mergedKO.vcf.gz.log'
    shell:
        'java -jar {input.picard} MergeVcfs I={input.KO1} I={input.KO2} I={input.KO3} D={input.ref} O={output} 2> {log}'

rule decompres_gz:
    input:
        'merged/mergedKO.vcf.gz'
    output:
        'merged/mergedKO.vcf'
    log:
        'logfiles/merged/mergedKO.vcf.log'
    shell:
        'gunzip -c {input} > {output} 2> {log}'

rule decomposing_WT:
    input:
        'zipped_vcf/WT.vcf.gz'
    output:
        'zipped_vcf/WT.vcf'
    log:
        'logfiles/zipped_vcf/WT.vcf.log'
    shell:
        'gunzip -c {input} > {output} 2> {log}'
