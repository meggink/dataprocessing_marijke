# convert 2bit to fasta file
rule twoBit2Fa:
    input:
        'data/mm10.2bit'
    output:
        'data/mm10.fa'
    log:
        'logfiles/data/mm10.fa.log'
    shell:
        'twoBitToFa {input} {output} 2> {log}'

# index fasta file
rule index_fasta:
    input:
        'data/mm10.fa'
    output:
        'data/mm10.fa.fai'
    log:
        'logfiles/data/mm10.fa.fai.log'
    shell:
        'samtools faidx {input} 2> {log}'

# sort the bam files
rule samtools_sort:
    input:
        'data/{sample}.bam'
    output:
        'sorted_data/{sample}.bam'
    log:
        'logfiles/orted_data/{sample}.bam.log'
    threads: 10
    shell:
        'samtools sort {input} > {output} --threads {threads} 2> {log}'

# index the bam files
rule samtools_index:
    input:
        'sorted_data/{sample}.bam'
    output:
        'sorted_data/{sample}.bam.bai'
    log:
        'logfiles/sorted_data/{sample}.bam.bai.log'
    threads: 10
    shell:
        'samtools index {input} 2> {log}'
