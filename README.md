# Dataprocessing: Genotype variants pipeline
<hr>
Dit project bevat de oefenopgaven voor het vak Dataprocessing, en in de map eindopdracht zit een pipline voor 
visualisatie van RNA sequentie varianten van een muis mm10 build. 

![dag](eindopdracht/dag.png)
In de afbeelding hierboven is een overzicht van alle stappen die de pipeline bevat.

### Packages 
<hr>
Dit project wordt allemaal gedaan in een conda environment. Via deze link kan conda worden geïnstalleerd:
https://docs.conda.io/en/latest/miniconda.html <br>

Vervolgens kan je een conda environment aanmaken, vanuit daar wordt veder gewerkt in het project.
```shell
# environment aanmaken
conda update conda
conda create --name <name> python

# activeer en install de juiste libraries
conda activate <name>
conda config --env --add channels conda-forge
conda config --env --set channel_priority strict
conda install python=3
conda update python
```
Voor variant calling wordt freebayes en vcflib gebruikt. Deze kunnen beide worden geïnstalleerd met conda:
```shell
conda install -c bioconda freebayes
conda install -c bioconda vcflib
```
De pipeline wordt gemaakt met snakemake, dit wordt met pip geïnstalleerd:
```shell
pip3 install snakemake
```
Het faToTwoBit script kan voor linux en macOSX gebruikers hier worden gedownload: http://hgdownload.cse.ucsc.edu/admin/exe/

Voor het mergen van de vcf files wordt picard gebruikt. Dit kan hier worden gedownload: 
https://github.com/broadinstitute/picard/releases/tag/2.26.9 <br>
Plaats deze file in een map scrips

### Data
<hr>
Het Mouse Reference Genome mm10 is nodig: http://hgdownload.cse.ucsc.edu/goldenpath/mm10/bigZips/mm10.2bit

De bam files met de alignments kunnen hier worden gedownload: 
https://usegalaxy.org/u/carlosfarkas/h/test-sall2-ko-rna-seq-gse123168-1

### Pipeline
<hr>
De pipeline kan worden gerund vanuit de eindopdracht map met de volgende syntax: 

```shell
snakemake --snakefile Snakefile --cores 40
```

Hiermee worden de rules die in de snakefiles staan een voor een aangeroepen.

