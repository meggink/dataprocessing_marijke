#configfile: 'config.yaml'

rule bcftools_call:
    input:
        fa = 'genome.fa',
        bam = expand('sorted_reads/{sample}.bam', sample=config["samples"]),
        bai = expand('sorted_reads/{sample}.bam.bai', sample=config["samples"])
    output:
        'calls/all.vcf'
    shell:
        'samtools mpileup -g -f {input.fa} {input.bam} | bcftools call -mv - > {output}'
